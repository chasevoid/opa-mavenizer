package com.monadsolutions.simpleproject;

import com.oracle.determinations.engine.Attribute;
import com.oracle.determinations.engine.Engine;
import com.oracle.determinations.engine.Entity;
import com.oracle.determinations.engine.EntityInstance;
import com.oracle.determinations.engine.Rulebase;
import com.oracle.determinations.engine.Session;

public class Main {

	public static void main(String[] args) {
		Rulebase rulebase = Engine.INSTANCE.getRulebase("src/main/resources/SimpleBenefits.zip");
		Session session = Engine.INSTANCE.createSession(rulebase);
		
		EntityInstance global = session.getGlobalEntityInstance();
		
		String childEntityPublicName = "child";
		Entity childEntity = rulebase.getEntity(childEntityPublicName);
		
		String childAgeAttributePublicName = "child_age";
		Attribute childAgeAttribute = rulebase.getAttribute(childAgeAttributePublicName, childEntityPublicName);
		
		String childIsATeenagerAttributePublicName = "child_teenager";
		Attribute childIsATeenagerAttribute = rulebase.getAttribute(childIsATeenagerAttributePublicName, childEntityPublicName);
		
		EntityInstance child1EntityInstance = session.createEntityInstance(childEntity, global);
		int child1Age = 15;
		childAgeAttribute.setValue(child1EntityInstance, child1Age);
		
		EntityInstance child2EntityInstance = session.createEntityInstance(childEntity, global);
		int child2Age = 22;
		childAgeAttribute.setValue(child2EntityInstance, child2Age);
		
		session.think();
		
		System.out.println("Child aged " + child1Age + " is a teenager: " + childIsATeenagerAttribute.getValue(child1EntityInstance));
		System.out.println("Child aged " + child2Age + " is a teenager: " + childIsATeenagerAttribute.getValue(child2EntityInstance));
	}
}
